var api_url = 'http://isaac.metajua.gq/api.php';

var current_task = {
    id: null,
    name: null,
    description: null
};

var db = window.openDatabase( 'todoApp', '0.0.1', 'Todo App', 100000 );

function createTable() {
	//create a table in sqlite
	db.transaction( function( tx ) {
		tx.executeSql( 
			'CREATE TABLE tasks ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name CHAR(250), description TEXT, start_datetime DATETIME, end_datetime DATETIME, status INTEGER, last_updated DATETIME )',
			[],
			function( tx, result ) {
				window.localStorage.setItem( 'table_created', true );
			},
			null
		);
	}, null );
}


//function
function getTimestamp( date ) {
	date = typeof date == 'undefined' ? ( new Date() ) : date;

	var timestamp = '' + date.getFullYear(),
		month = ( date.getMonth() + 1 ),
		day = date.getDate(),
		hours = date.getHours(),
		minutes = date.getMinutes(),
		seconds = date.getSeconds();

	timestamp += '-' + ( month < 10 ? '0' + month : month ) + '-' + ( day < 10 ? '0' + day : day );
	timestamp += ' ' + ( hours < 10 ? '0' + hours : hours ) + ':' + ( minutes < 10 ? '0' + minutes : minutes ) + ':' + ( seconds < 10 ? '0' + seconds : seconds );

	return timestamp;
}

function query_update(){
	var get_data = {
		'action':'get_tasks'
	};
	console.log(get_data);
	//query the server
	/*$.ajax({
		type:"post",
		url: 'isaac.metajua.gq/api.php',
		data: get_data,
		success:function(result){
			console.log(result);
		},
		error: function(){
			console.log('an error occured');
		}
	});*/

}



function show_tasks() {
	

	//hide_all_screens();
	//query_update();
	
	
	current_task.id = null;

	db.transaction( function( tx ) {
		tx.executeSql(
			'SELECT * FROM tasks ORDER BY id DESC', 
			[],
			function( tx, result ) {
				var html = '';

				if(result.rows.length > 0){
				
						for ( i = 0; i < result.rows.length; i++ ) {
							var record = result.rows.item( i );
							
							html += '<li data-task-id="' + record.id + '">' +
								'<a href="#task_details">' +
									record.name +
								'</a>' +
								' | <span>Delete</span>' +
							'</li>';
						}

				} else { alert('no tasks'); }

				$( '#tasks' ).html( html ); 
				
				//$( '#show_tasks' ).show();

				$( '#tasks li a' ).on( 'click', function() {
					var task_id = $( this ).parent( 'li' ).attr( 'data-task-id' );

					task_details( task_id );
				} ); 
	
				$( '#tasks li span' ).on( 'click', function() {
					var task_id = $( this ).parent( 'li' ).attr( 'data-task-id' );

					db.transaction( function( tx ) {
						tx.executeSql( 
							'DELETE FROM tasks WHERE id = ?',
							[ task_id ],
							function( tx, result ) {
								$( '#tasks li[data-task-id="' + task_id + '"]' ).remove();
							},
							null
						);
					}, null );
				} );
			},
			null
		);
	}, null );
	
    //$( '#show_tasks > a' ).off( 'click' ).on( 'click', function() {
      //  add_task();
   // } );  
}




function task_details( task_id ) {
	//hide_all_screens();
	
	db.transaction( function( tx ) {
		tx.executeSql( 
			'SELECT * FROM tasks WHERE id = ? LIMIT 1',
			[ task_id ],
			function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var record = result.rows.item( 0 );
					
					current_task.id = record.id;
					current_task.name = record.name;
					current_task.description = record.description;

					//$( '#task_details' ).show();

					var status = [ 'Not complete', 'Complete', 'Ongoing' ];

					var html = '' +
						'<li>' +
							'<h4>Name</h4>' +
							'<p>' + record.name + '</p>' +
						'</li>' +
						'<li>' +
							'<h4>Status</h4>' +
							'<p>' + status[ record.status ] + '</p>' +
						'</li>' +
						'<li>' +
							'<h4>Description</h4>' +
							'<p>' + record.description + '</p>' +
						'</li>' +
					'';

					$( '#details' ).html( html );
				}
			},
			null
		);
	}, null );

    $( '#task_details > a:nth-child(2)' ).off( 'click' ).on( 'click', function() {
        add_task();
    } );

    $( '#task_details > a:last-child' ).off( 'click' ).on( 'click', function() {
        show_tasks();
    } );
}

function add_task() {
   // hide_all_screens();

    $( '#add_task h2 span' ).text( current_task.id == null ? 'Add' : 'Edit' );
	
	$( '#task_name' ).val( '' );
    $( '#task_description' ).val( '' );

    if ( current_task.id != null ) {
        $( '#task_name' ).val( current_task.name );
        $( '#task_description' ).val( current_task.description );
    }

    //$( '#add_task' ).show();
	//$( '#save_task' ).on( 'click', function() {
		


    $( '#add_task > a' ).off( 'click' ).on( 'click', function() {
        show_tasks();
    } );
}




function hide_all_screens() {
    $( '#screens > div' ).hide();
}

//hide_all_screens();

show_tasks();



function synchronizeData() {
	db.transaction( function( tx ) {
		tx.executeSql(
			'SELECT * FROM tasks WHERE remote_id IS NULL LIMIT 1',
			[],
			function( tx, result ) {				
				if ( result.rows.length > 0 ) {
					var record = result.rows.item( 0 );
					
					$.post(
						'http://isaac.metajua.gq/api.php', 
						{
							action: 'add_task',
							name: record.name,
							description: record.description,
							start_datetime: record.start_datetime,
							end_datetime: record.end_datetime
						}, 
						function( response ) {
							if ( response.result == 'successful' )
								db.transaction( function( tx ) {
									tx.executeSql( 
										'UPDATE tasks SET remote_id = ? WHERE id = ?',
										[ response.remote_id, record.id ],
										function( tx, result ) {
											synchronizeData();
										},
										function( tx, error ) {
											synchronizeData();
										}
									);
								}, null );
								
							else
								synchronizeData();
						},
						'json' 
					).fail( function() {
						synchronizeData();
					} );
				}
				
				else
					synchronizeData();
			},
			null
		);
	}, null );
}

function saveTask(){

		$( '#add_task' ).off( 'click' ).on( 'click','#save_task', function() {
		
        var name_element = $( '#task_name' );
        var description_element = $( '#task_description' );

        if ( name_element.val() != '' ) {
            if ( current_task.id == null ) {
				db.transaction( function( tx ) {
					var now = new Date();				
						var start_datetime = getTimestamp( now );
					
					now.setDate( now.getDate() + 2 );
						var end_datetime = getTimestamp( now );
						
					tx.executeSql( 
						'INSERT INTO tasks ( name, description, start_datetime, end_datetime, status, last_updated ) VALUES ( ?, ?, ?, ?, ?, ? )',
						[ name_element.val(), description_element.val(), start_datetime, end_datetime, 0, start_datetime ],
						function( tx, result ) {
							show_tasks();
							
							name_element.val( '' );
							description_element.val( '' );
						},
						null
					);
				}, null );
            }

            else {
                db.transaction( function( tx ) {
					var timestamp = getTimestamp();
						
					tx.executeSql( 
						'UPDATE tasks SET name = ?, description = ?, last_updated = ? WHERE id = ?',
						[ name_element.val(), description_element.val(), timestamp, current_task.id ],
						function( tx, result ) {
							show_tasks();
							
							name_element.val( '' );
							description_element.val( '' );
						},
						null
					);
				}, null );
            }
        }

        else {
            name_element.focus();
        }
		$( this ).dialog( 'close' );
    } );
}




document.addEventListener( 'deviceready', function() {
	if ( window.localStorage.getItem( 'table_created' ) == null )
		createTable();
		
	saveTask();
	synchronizeData();
} );